import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Auctions from './components/Auctions'
import AuctionDetail from './components/AuctionDetail'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom'


class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">          
          <ul>
            <li><Link to="/auctions">auctions</Link></li>            
          </ul>
          <Switch>
            <Route path="/auctiondetail" component={AuctionDetail} />
            <Route path="/auctions" component={Auctions} />
          </Switch>          
        </div>
      </Router>
    );
  }
}

export default App;
