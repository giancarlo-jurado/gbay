import React, { Component } from 'react';
//import { HubConnection, HttpTransportType, ConsoleLogger, LogLevel } from '@aspnet/signalr';
import * as SignalR from '@aspnet/signalr';
import { Table } from 'react-bootstrap';



class Auctions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            auctions: [],
            requestFailed: false
        };
    }

    componentDidMount() {        
        const hubConnection = new SignalR.HubConnectionBuilder()
            .withUrl("http://localhost:53686/hubs/auction")
            .configureLogging(SignalR.LogLevel.Information)
            .build();        

          hubConnection.on("AuctionAdded", (value) => {
            this.setState({
                auctions: [...this.state.auctions, value]
            })
          });

          hubConnection.on("BidPlaced", (value) => {
            let auctionsCopy = this.state.auctions;
            var auction = auctionsCopy.find(x=> x.id === value.auctionId);
            auction.highestBid = value.amount;
            auction.bids++;
            
            this.setState({
                auctions: auctionsCopy
            })
          });
          hubConnection.start();

        this.getAuctions();
    }

    getAuctions() {
        fetch('http://localhost:53686/api/auction')
            .then(response => {
                if (!response.ok) {
                    throw Error("Network request failed")
                }

                return response
            })
            .then(d => d.json())
            .then(d => {
                this.setState({
                    auctions: d
                })
            }, () => {
                this.setState({
                    requestFailed: true
                })
            })
    }

    handleClick (item) {        
        console.log('Item', item);
        this.props.history.push({
            pathname: '/auctiondetail',            
            state: { item: item }
          });
      }

    render() {        
        if (this.state.requestFailed) return <p>Failed</p>
        if (!this.state.auctions) return <p>Loading...</p>
        return (
            <div>
                <Table responsive hover>
                    <thead>
                        <tr>
                            <th colSpan="3">
                                <h2>Current Auctions</h2>
                            </th>
                        </tr>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Highest Bid</th>
                            <th>Bids</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.auctions.map(item => (
                                <tr onClick={this.handleClick.bind(this, item)}>                                
                                    <td>{item.item.title}</td>
                                    <td>{item.item.description}</td>
                                    <td>{item.highestBid > 0 ? item.highestBid : 'None'}</td>
                                    <td>{item.bids}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default Auctions;