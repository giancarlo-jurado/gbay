import React, { Component } from 'react';
//import { HubConnection, HttpTransportType, ConsoleLogger, LogLevel } from '@aspnet/signalr';
import * as SignalR from '@aspnet/signalr';
import { Table, Grid, Row, Col, Badge } from 'react-bootstrap';



class AuctionDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bids: [],
            requestFailed: false
        };
        console.log("this.props.location.state.item.id", this.props.location.state.item.id);
    }



    componentDidMount() {
        const hubConnection = new SignalR.HubConnectionBuilder()
            .withUrl("http://localhost:53686/hubs/auction")
            .configureLogging(SignalR.LogLevel.Information)
            .build();


        hubConnection.on("BidPlaced", (value) => {
            this.setState({
                bids: [...this.state.bids, value]                
            });
            this.props.location.state.item.bids++;
            this.props.location.state.item.highestBid = value.amount;
        });
        hubConnection.start();

        this.getBids();
    }

    getBids() {
        fetch(`http://localhost:53686/api/bid?auctionId=${this.props.location.state.item.id}`)
            .then(response => {
                if (!response.ok) {
                    throw Error("Network request failed")
                }

                return response
            })
            .then(d => d.json())
            .then(d => {
                this.setState({
                    bids: d
                })
            }, () => {
                this.setState({
                    requestFailed: true
                })
            })
    }
    render() {
        return (
            <div>
                <h2>Auction Details</h2> {this.props.location.state.item.id}
                <div className='row'>
                    <div className='col-sm-2 pull-right'>
                        Title:
                        </div>
                    <div className='cole-sm-6'>
                        {this.props.location.state.item.item.title}
                    </div>
                </div>
                <div className='row'>
                    <div className='col-sm-2 pull-right'>
                        Description:
                        </div>
                    <div className='cole-sm-6'>
                        {this.props.location.state.item.item.description}
                    </div>
                </div>
                <div className='row'>
                    <div className='col-sm-2 pull-right'>
                        Starting Price:
                        </div>
                    <div className='cole-sm-6'>
                        {this.props.location.state.item.item.price}
                    </div>
                </div>
                {/* <div className='row'>
                    <div className='col-sm-2 pull-right'>
                        Current Highest Bid:
                        </div>
                    <div className='cole-sm-6'>
                        {this.props.location.state.item.highestBid}
                    </div>
                </div>
                <div className='row'>
                    <div className='col-sm-2 pull-right'>
                        Current Bids:
                        </div>
                    <div className='cole-sm-6'>
                        <span class="badge badge-secondary">{this.props.location.state.item.bids}</span>
                    </div>
                </div> */}
                <div className='row'>
                    <Table responsive hover>
                        <thead>                            
                            <tr>
                                <th>User</th>
                                <th>Amount</th>
                                <th>Date Bid</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.bids.map(item => (
                                    <tr>
                                        <td>{item.userId}</td>
                                        <td>{item.amount}</td>
                                        <td>{item.createdDate}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </div>
            </div>
        )
    }
}

export default AuctionDetail;